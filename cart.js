const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]



// Q1. Find all the items with price more than $65.

function itemPrice(item) {
    let arr = Object.entries(item[0]);
    let price = arr.filter((curr) => {
        if (!Array.isArray(curr[1])) {
            let prices = parseInt(curr[1]['price']
                .replace('$', ''));
            return prices > 65;
        } else {
            let prices = curr[1].filter(item=> {
                let utensil = Object.entries(item)
                let price = parseInt(utensil[0][1].price
                    .replace('$', ''))
                return price > 65;
            });
            console.log(prices)
        }
    })
    return price;
}
console.log(itemPrice(products));

// Q2. Find all the items where quantity ordered is more than 1.

function quantity(item) {
    let arr = Object.entries(item[0]);
    let quantity = arr.filter((curr) => {
        if (!Array.isArray(curr[1])) {
            return curr[1]['quantity'] > 1;
               
        } else {
            let quant = curr[1].filter(item=> {
                let utensil = Object.entries(item)
                return utensil[0][1]["quantity"] > 1;     
            });
            console.log(quant);
        }
    })
    return quantity;
}
console.log(quantity(products));

// Q.3 Get all items which are mentioned as fragile.
// Q.4 Find the least and the most expensive item for a single quantity.
// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

